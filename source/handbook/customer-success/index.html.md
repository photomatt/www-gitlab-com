---
layout: markdown_page
title: "Customer Success"
---

## Reaching the Solutions Architect Team (internally)

* [Public Issue Tracker] (https://gitlab.com/gitlab-com/customer-success/issues); please use confidential issues for topics that should only be visible to team members at GitLab.
* You can also send an email to the Solutions Architect team via sa-team@gitlab.com
* Chat channel; please use the #solutions_architect chat channel for questions that don't seem appropriate to use the issue tracker or the internal email address for.

## On this page



## Other Customer Success Topics

* [Solutions Architect Onboarding]
* [Sales Standard Operating Procedures](http://about.gitlab.com/handbook/sales/sop/)
* [Sales Operations](https://about.gitlab.com/handbook/sales/salesops/)
* [Sales Skills Best Practices](https://about.gitlab.com/handbook/sales-training/)
* [Sales Discovery Questions](https://about.gitlab.com/handbook/sales-qualification-questions/)
* [EE Product Qualification Questions](https://about.gitlab.com/handbook/EE-Product-Qualification-Questions/)
* [GitLab Positioning](https://about.gitlab.com/handbook/positioning-faq/)
* [FAQ from prospects](https://about.gitlab.com/handbook/sales-faq-from-prospects/)
* [CLient Use Cases](https://about.gitlab.com/handbook/use-cases/)
* [POC Template](https://handbook/sales/POC/)
* [Account Planning Template for Large/Strategic Accounts](https://docs.google.com/presentation/d/1yQ6W7I30I4gW5Vi-TURIz8ZxnmL88uksCl0u9oyRrew/edit?ts=58b89146#slide=id.g1c9fcf1d5b_0_24))
* [Sales Demo](/handbook/sales/demo/)
* [Idea to Production Demo](/handbook/product/i2p-demo)
* [Sales Development Team Handbook](/handbook/sales/sdr)
* [Who to go to to ask Questions or Give Feedback on a GitLab feature](https://about.gitlab.com/handbook/product/#who-to-talk-to-for-what)
* [CEO Preferences when speaking with prospects and customers](https://about.gitlab.com/handbook/people-operations/ceo-preferences/#sales-meetings)
 

### Sales Resources outside of the Sales Handbook

* [Resellers Handbook](/handbook/resellers/)
* [Customer Reference Sheet](https://docs.google.com/a/gitlab.com/spreadsheets/d/1Off9pVkc2krT90TyOEevmr4ZtTEmutMj8dLgCnIbhRs/edit?usp=sharing)
* [Sales Collateral](https://drive.google.com/open?id=0B-ytP5bMib9TaUZQeDRzcE9idVk)
* [Sales Training](https://drive.google.com/open?id=0B41DBToSSIG_WU9LZ0o0ektEd0E)
* [Lead Gen Content Resources](https://about.gitlab.com/resources/)
* [GitLab University](https://docs.gitlab.com/ce/university/)
* [Our Support Handbook](https://about.gitlab.com/handbook/support/)
* [GitLab Hosted](https://about.gitlab.com/gitlab-hosted/)
 

### Sales Team Call

1. The Sales team call is every Monday 9:00am to 9:30am Pacific Time.
2. We cover several areas: where we are at with revenue, sales operation updates to help productivity, how the BDR and SDR team are contributing to our pipleine, product marketing sharing how they are enabling sales productivity and scrum.
1. We use [Zoom](https://zoom.us) for the call since Hangouts is capped at 15 people, link is in the calendar invite, and also listed at the top of the Sales Team Agenda.
1. The call is recorded automatically, and all calls are transferred every hour to a Google Drive folder called "GitLab Videos". There is a subfolder called "Sales Team Meeting", which is accessible to all users with a GitLab.com e-mail account.
1. We start on time and will not wait for people.
1. Person who has first item on the agenda starts the call.
1. If you are unable to attend just add your name to the [Sales Team Agenda](https://docs.google.com/document/d/1l1ecVjKAJY67Zk28CYFiepHAFzvMNu9yDUYVSQmlTmU/edit) as 'Not attending'.
1. We start by discussing the subjects that are on the agenda for today.
   * Everyone is free to add subjects. Please start with your name and be sure to link to an issue, merge request or commit if that is relevant.
   * When done with a point mention the subject of the next item and hand over to the next person.
   * When someone passes the call to you, no need to say, “Can you hear me?” Just begin talking. If we can’t hear you, we’ll let you know.
1. Even if you cannot join the call, consider reviewing the recorded call or at minimum read through the sales team agenda and the links from there.
 

### Sales Brown Bag Training Call

1. When: Happens every Thursday at Noon EDT
2. Where: https://gitlab.zoom.us/j/354866561
3. What: [Agenda](https://docs.google.com/document/d/1ygbqKyjxJvD195MTtDo53WlCk4idVQHZta3Onmh2vAc/edit)

### Pitches

#### Elevator pitch

GitLab is an integrated product for the entire software development life-cycle.
It contains not only issue management, version control and code review but also CI, CD, and monitoring.
2/3 of the organizations that self-host git use GitLab, that are more than 100,000 organizations and millions of users.

#### Is it similar to GitHub?

GitLab started as an open source alternative to GitHub.
Instead of focusing on hosting open source projects we focused on the needs of the enterprise, for example we have 5 authorization levels vs. 2 in GitHub.
Now we've expanded the feature set with continuous integration, continuous delivery, and monitoring.

#### What is the benefit?

We help organizations reduce the cycle time between having an idea and seeing it in production.
Before GitLab you needed 7 tools, it took a lot of effort to integrate them, and you end up with have different setups for different teams.
With GitLab you get data on how long each part of the software development life-cycle is taking and how you can improve it.

#### [GitLab Positioning](https://about.gitlab.com/handbook/positioning-faq/)

### GitLab Version Check

Before prospecting and engaging with a prospect, check to see if they are using CE. To do this, use GitLab Version Check. Everything about [GitLab Version Check](/handbook/sales-process/version_check).


### Market segmentation
Sales segments the market based on IT or [TEDD](https://discoverorg.com/discoverorg-sales-intelligence-platform/data-sets-for-marketing-sales-and-staffing/technology-engineering-design-and-development-tedd-dataset/) employees
1. Strategic: 5000+ employees
2. Large: 750 - 4999 employees
3. Mid Market: 100 - 749 employees
4. SMB: less than 100 employees

We have two sales positions focused on selling into these markets.
1. Sr. Account Executive for Strategic and Large Accounts
2. Account Executive for Mid Market Accounts
3. SMB is our transactional business, managed by the Business Development Team and processed through our web portal.

Expected quota for each sales segment is:
1. Strategic Account Leader - Strategic/Large Accounts is $1.5M incremental annual contract value (ACV)
3. Account Executive - Large/Mid-Market accounts ranges from  $700,000 - $1,000,000 incremental ACV


### Mission Statement
To deliver value to all customers by engaging in a consistent, repeatable, scalable way across defined segments so that customers see the value in their investment, and we retain and drive growth in our customer base
The mission of the solutions architect team is to provide these customers with experience in order to:
1. Accelerate initial customer value
2. Maximize long-term, sustainable customer value
3. Improve overall customer satisfaction & referenceability
4. Maximize the total value of the customer to GitLab


### Problem Statement
Our large and strategic customers are in need of an ongoing partnership that combines expert guidance with flexibility and adaptability to support their adoption and continuous improvement initiatives.  
These customers expect that partner to provide a streamlined, consistent and fully coordinated experience that encompasses the full span of their needs as well as the fully lifecycle of the relationship.
Need to focus on 3 main areas in order to grow in our existing accounts as well as land large and strategic:

1. Awareness
2. Adoption
3. Usage


### Initiative: Awareness
Opportunity to improve the overall awareness of GitLab in order to promote and evangelize our brand and solution in a meaningful way to provide big business impact to our customers so that they beleive in our vision and strategy 

### Initiative: Adoption
Ensuring paying customers are successful in their onboarding in order to gain adoption and get the most out of our platform and remain happy, paying GitLabers and brand advocates.

### Initiative: Usage
Collecting and making use of customer data and insights is key to customer success.  It’s important to do more with data and when necessary share back with customers, which in turn helps and encourages our customers to improve and drive adoption.



## Customer On-boarding Checklist

1.	**Welcome!** — Welcome the customer aboard GitLab, introduce yourself and your purpose within the company and how you will support them throughout the relationship with the business and what they can expect from you. Provide the introduction to support and how to obtain support "For enterprise, Enterprise Edition receives next business day support via e-mail. Please submit your support request through the [support web form](https://gitlab.zendesk.com/hc/en-us/requests/new).
2.	**Personal introduction** — (2-5 days) Create a task in Salesforce to conduct a personal introduction to your new account contacts. This should be quick and informal phone call, this is not an opportunity for discovery but an outreach for you to build rapport and open lines for communication.
3.	**Installation follow-up** — (7-10 days) Create a task in Salesforce to follow-up on the installation, was it successful did they have encounter any issues, were the issues self resolved by using documentation or was the help of support required. This time is a good opportunity to share with the customer the vision of [GitLab "Idea to Production"](https://youtu.be/ZRcWCWatdas) YouTube video.
4.	**Education** — (20-30 days) Create a task in Salesforce for product education. Remind your customer that our releases are on the 22nd of each month per our [Direction](https://about.gitlab.com/direction/). Depending on your timing of this correspondence you may inform them of any major enhancements that have been released or are about to be released.
5.	**Discovery** — (60 days) Now that your customer has had some time and experience using GitLab, set out to discover the need for EE features and products by running through [EE Product Qualification Questions](/handbook/EE-Product-Qualification-Questions/).
6.	**Check-in** — (90 days) Create a task in salesforce for check-in with customer. Ask if the customer has any outstanding issues. Do they have any feature requests?  This is also an opportunity to identify if there has been any changes in the organization, or an opportunity for further user adoption for their goals. For a status check, also identify that key decision makers and license contacts are still current.  
7.	**Outlook** — (6 months) Same as 90 day Check-in task, additionally discuss what the customer roadmap and outlook looks like for the next 6 months. What can we expect in terms terms of growth, what does the customer expect in terms of our product and offerings.
8.	**Renewal/Expansion** — (10 months) Check in with the customer and let them know they are soon due for renewal. Are there any changes to who is responsible for the renewal or otherwise? Good time to ask about their team growth to see if they need more seats.  Good time to educate and develop need for GitLab Products.
9.	**Renewal** — (11 months) Check in with the customer if they have not yet renewed, if there are any blockers to renewal or any changes to expect.
10.	**Renewal** — (12 months) Follow up with the customer, if we have lost their renewal discover the reasons why we did not succeed and if any changes can be made or improved. If they have moved to another solution, which and why?
11. **Expansion** —  For Strategic Deals, an [Account Expansion Template](/handbook/account-management/account-expansion/) should be created.


## Customer Roles
Customer Success has two roles assigned to account coverage - Solutions Architect (SA) and Account Manager (AM).  For definitions of the account size, refer to [market segmentation](/handbook/sales#market-segmentation) which is based on the _Potential EE Users_ field on the account.

- Strategic: Sr. Account Executive closed deal and stays on account for growth and renewal.  Solutions Architect is assigned pre-sales and stays on the account to help with adoption and growth.
- Large: Sr. Account Executive closed deal and stays on account for growth and renewal.  Solutions Architect is assigned pre-sales and stays on the account to help with adoption and growth.
- Mid-Market: Account Executive closes deal and transfers to Account Manager to handle renewals and growth. The Account Manager records the Previous Account Owner on the Opportunity Team in any deals while the account is in the New Customer period within the first 12 months including the first annual renewal.
- SMB: Web Portal with Sales Admin oversight. 

## When to engage a Solutions Architect

- Customer/prospect has been identified as
   - Key customer
   - Has or is interested in more than 100 licenses
   - Premium Support customer
- Customer has specific challenges with GitLab and could use an internal advocate to manage them
   - Feature requests
   - Performance / configuration questions
   - Political
        - Solutions Architects may have a different entry point into the corporation with another group
   - Assistance in migration from another tool/tools
   - Advanced GitLab/Git usage questions
   - Product demos
   - Other

## How to engage a Solutions Architect

- Identify a Solutions Architect in the appropriate time zone / region
- Discuss technical challenges of customer/prospect with Solutions Architect via email/hangout/Slack/etc.
- If desirable, schedule a call with the customer/prospect to introduce the Solutions Architect




