---
layout: markdown_page
title: "Interviewing"
---

## How to Apply for a Position

The best way to apply for a position with GitLab is directly through our [jobs page](/jobs), where all open positions are advertised. If you do not see a job that aligns with your skillset, please keep your eye on the jobs page and check back in the future as we do add roles regularly. Please be advised that we do not retain unsolicited resumes on file, so you will need to apply directly to any position you are interested in now or in the future.

To apply for a current vacancy:

1. Go to our [jobs page](/jobs).
1. Mid-way down the page, there is a section called "Available Openings".
1. Click on the position title that interests you!
1. This will take you to the job description.
1. If this position interests you, click the "Apply" button at the bottom of page.
1. You will be redirected to a new page hosted by Workable, our Applicant Tracking System (ATS), that asks you to fill out basic personal information, provide your resume and cover letter, and answer any application questions.
1. Once you have finished, click "Submit your application" at the bottom.

## Typical Hiring Timeline

An applicant should expect to wait 2-3 business days between each step of the process. An applicant, at any time, is welcome to contact the People Ops team member they are working with for an update on their candidacy.

1. **Confirmation of the application**: Applicants automatically receive confirmation of their application, thanking them for submitting their information. This is an automated message from Workable. If the person came through another channel they must be added to Workable before continuing the process. There are various ways to do this, see [Workable's documentation](https://resources.workable.com/adding-candidates).
1. The employment team does a **first round of evaluations**. Disqualified candidates will be sent a note informing them of the [rejection](#rejecting-applicants). There are templates in Workable to assist, but messages can be tailored as appropriate: place yourself on the receiving end of the message. If more information is required to make the determination, feel free to specifically ask for it (e.g. a cover letter). (If you don't see the templates, you probably haven't [linked your email account to Workable](https://resources.workable.com/support/connecting-gmail-with-workable) yet.)
1. **Pre-screening Questionnaire**: Many applicants will be sent a pre-screening questionnaire by the employment team relating to the position to complete and return to the sender. Please do not reply "in-line" to the email, as Workable is unable to upload in-line responses. The questionnaire and answers will be added to the candidate's Workable profile.
   1. Team members who review the pre-screening questionnaire answers should refer to the [GitLab private project](https://gitlab.com/gl-peopleops/Applicant-Technical-Questionnaire-Guides) that holds guides on how to review each of the questionnaires. If you are a reviewer and need access to this project, please reach out to People Ops.
1. [**Screening call**](#screening-call): in Workable,
   1. If the applicant qualifies, a member of the employment team will send the applicant the Calendly link for the Recruiters using the template in Workable.
   1. A member of the employment team will move the applicant to the "screening call" stage in Workable.
   1. Our [Global Recruiters](https://about.gitlab.com/jobs/recruiter/) will do a screening call;
Depending on the outcome of the screening call, the employment team or manager can either [reject an applicant](#rejecting-applicants) or move the applicant to the interview stage in Workable.
1. **Technical interview (optional)**: As described on the [Jobs](/jobs/) page, certain positions require [technical interviews](/handbook/hiring/interviewing/technical).
1. **Further interviews**: Additional interviews would typically follow the reporting lines up to the CEO. So for instance, the technical interview would be by a co-worker, next interviews would be with the manager / team lead, executive team member, and then the CEO. See below for [sample questions](#interview-questions). The candidate should be interviewed by at least one female GitLab team member.
1. **Reference calls**: The employment team will make [reference calls](#reference-calls) for promising candidates. This process can start at an earlier stage, but should happen before an offer is made. At minimum two reference calls should be completed - one to a manager, the other to a colleague. Move the candidate to the "Reference Call" stage in Workable, and ping the relevant person from People Ops to get it going.
1. **Hiring package**: After reference calls are completed successfully, the employment team submits the [employee approval package](#hiring-package) to the CEO and Hiring Manager.
1. When the manager or CEO makes the **offer**, this can be done verbal during the call with the applicant, but is always followed quickly with the written offer as described in [the section on preparing offers and contracts](#prep-contracts).
1. People Ops will draft a contract based upon the written offer that was extended.
1. Manager follows up to ensure that the offer is accepted, and that the contract is signed.
1. People Ops [starts the background check process](/handbook/people-operations/#background-checks) if applicable for the role.
1. People Ops [starts the onboarding process](#move-to-onboarding).
1. Manager considers [closing the vacancy](#closing-a-vacancy).

## Screening Call

We conduct screening calls for all positions. This call will be completed by our [Global Recruiters](https://about.gitlab.com/jobs/recruiter/).

Questions are:

1. Why are they looking for a new position?
1. Why did they apply with GitLab?
1. What is your experience with X? (do for each of the skills asked in the position description)
1. Current address? (relevant in context of comp, and in case of contract we will need that information)
1. Full legal name? (relevant in case an offer would be made)
1. How do they feel about working remotely and do they have experience with it?
1. Compensation expectation.
1. Notice period needed
1. Do you require a visa/residence permit to be able to work for GitLab?
1. STAR questions and simple technical questions may also be asked during the screening call if applicable.

[An example of the output of a good screening call](https://gitlab.workable.com/backend/jobs/128446/browser/applied/candidate/7604850) (need workable account).

At the end of the screening call applicant should be told what the timeline is for what the next steps are (if any).
An example message would be "We are still reviewing applications, but our goal is to let you know in 3 business days from today whether you've been selected for the next round or not. Please feel free to ping us if you haven't heard anything from us by then."

## Moving applicants through the process

**Remember to inform applicants about what stage they are in.** So, for example, if in the hiring process for the particular position / team you've agreed that there will be four stages, be sure to inform the applicant of where they are in the process during each call / stage: "You are in stage X and will be moving to stage Y next." Some brief feedback from the previous stage can also be included to help the candidate gauge their progress.

**The process can differ from team to team and from position to position.** If an applicant submits a resume to a particular position and is later moved into an open application, send a short update notifying them that their process may be slightly different or delayed.

**Send Calendly link of the next person in the process.** If you have decided to move the applicant on in the process, save time by sending the next interviewer's Calendly link to the applicant. This saves the time it would otherwise take the next interviewer to send the link.

**Compensation is discussed at start and end but not in between.** Compensation expectations are asked about during the [screening call](#screening-call). If the expectations seem unworkable to the manager or recruiter (based on what had been approved by the compensation committee at the [creation of the vacancy](#vacancy-creation-process)), then the recruiter can send a note to the candidate explaining that salary expectations are too far apart. If expectations are aligned, then the topic of compensation should not re-surface until an [offer is discussed internally](#offer-authorization). Following this guideline avoids conflating technical and team interviews with contract discussions and keeps the process flowing smoothly.

If the manager has a question about compensation, please ping the People Operations Specialist for review. If the question needs to be escalated, the People Operations Specialist will add the Sr. Director of People Operations to the conversation.

**The CEO authorizes all offers.** <a name="offer-authorization"></a>The manager proposes a suggestion for an offer (including bonus structure if applicable, etc., using the [global compensation framework](/handbook/people-operations/global-compensation)) as an internal comment in Workable and informs the CEO on its details depending on what is applicable.

Be aware that the visibility of internal comments in Workable can be switched between hiring team _managers_ only, or _all_ members of the hiring team.

If you have completed your portion of the hiring process and would like to stop receiving notifications about a candidate from Workable, remove yourself as a follower. To do this go to the candidate's profile, under timeline there are boxes with the photo of each follower. Uncheck your photo/name, and this will remove you from the Workable emails. You will be added to each candidate profile upon being mentioned, so you might have to remove yourself each time.

## Conducting a GitLab interview

Interviewing is hard for both sides. In less than one hour you both need to get to know each other and both will have to make the decision if you want to work with this person.
This is an effort to provide a set of guidelines to make interviewing a bit less traumatizing.

Note: So you are about to interview folks for a job at GitLab? Please take a moment to carefully read
[this document on keeping it relevant and legal, including a self-test](https://docs.google.com/document/d/1JNrDqtVGq3Y652ooxrOTr9Nc9TnxLj5N-KozzK5CqXw)(please note this document is internal to GitLab while we edit it to make it fit for general audiences). For example, if there is a gap in employment history on a CV, you can ask the candidate what they did during that time to keep their skills current. You may not ask why they were absent from work as it may be related to a medical or family issue which is protected information.

### Before the interview

* Screening - writing a good resume is an art, and not many people master it. When you read a resume look for evolution rather than buzzwords, and if something sparks your curiosity, ask.
* If the process is taking too long, apologize and explain what is going on. It is really frustrating to not hear anything from the other side, and then resume conversations like nothing has happened. Show respect for the time of the candidate.
* If the process ends at this stage, be kind, and if the interviewee asks for feedback, give honest constructive feedback that explains why have we taken our decision.

### During the interview

1. There is an unbalanced power relationship during the interview. The interviewer is in a powerful position. It will decide if this candidate will get an offer or not. Be mindful of this. Be as friendly and approachable as you can. Be frank about what is going on, explain how the interview is going to be and set clear expectations: tell it like it is. This has the added value of getting people comfortable (over time) and allows you to get much better data.
1. Communication is really hard, don't expect perfect answers. Every person is different and they will express things differently, they are not listening to your train of thought so they will say things differently than what you expect, work on approaching to what they are trying to say rather than demanding them to approach to you. Once you have an answer validate your assumptions by explaining to the interviewed what you understood and allow the candidate to correct your story.
1. Don't go checking for perfect theoretical knowledge that the interviewee can google when needed, or give a problem that took you 2 months to dominate yet you expect your interviewee to master in a 30 minutes conversation. Be fair.
1. Aim to at the end of the interview to know if you want to work with this person.
1. Interview for soft skills, really, do it. Pick some behavioral questions to get data on what has the candidate done before and how his behavior aligns to the company values. We are all going to be much happier if we all naturally agree on how things should be.
1. Consider having more people interviewing with you, different people see and value different things. More data helps making better decisions and ends up being a better use of interviewing time for both the candidate and the company.
1. Always let the interviewee ask questions at the end, and be frank in your answers.

### Technical interview considerations

1. Try to get a real sample of work (which we already do for developers by working on GitLab issues). Avoid puzzles or weird algorithm testing questions. Probing for data structures is fine as long as it is relevant to the job the person is going to do.
1. Be mindful of the background of the candidate, someone who knows 10 languages already (and some languages in particular, Perl for ex), may pick up Ruby in a second given the right chance. Don't assume that someone with a Java background will not be capable of moving to a different stack. Note that individual positions may have stricter requirements; the Backend Developer position [requires Ruby experience](/jobs/developer/#ruby-experience), for example.
1. Consider including non-engineering people to ask soft skills questions. Because technical people should be capable of talking to non-engineering just fine, we should assess it.

### Behavioral interview questions (STAR)

The goal of these questions is to get the candidate to share data on past experiences. Previous behavior is considered the best way to predict how a person is going to act in the future.

They usually start with the form "Can you tell me about a time when...". The kind of answer that we are looking for is to get a story that is structured following the Situation, Task, Action, Result.

There is no right answer, what matters here is to hear the candidate and gather data on how it is telling the story.

Some things to pay attention to:

* What was the candidate role? Was it a leader? A follower? Why?
* What is it highlighting as important? Did it matter?
* Is it clearly explained? Is the story well told? If it is a technical story and the interviewer is a non-technical person, are things being explained in a way that make sense?
* If you ask "why?" will you get an actual explanation?
* Are the "Task" and the "Action" clear? Are they well reasoned?
* Is there a result or was the story left unfinished? Is it still going on?
* Was the result measured in any way? How does the candidate know that the result matches the expectation? Was there an expectation?

These questions can be quite unbalancing and can increase the stress during the interview. Again, be kind and help the candidate understand what are you looking for, provide a sample if it is needed and you notice that the candidate is blocked.

It can also happen that the candidate does not have a story to share with you, that is OK. It's just another data point that should be added to the feedback (I failed to get data on ...), just move to the next question, just be sure to have a few questions as a backup.

These questions should be aligned with our company values. What we are looking for is understanding how this candidate behaves, and if this behavior matches the one we look for in our company values.

Once you have your notes, tell the candidate what you understood, repeat the story, and let them correct you as needed.

For a brief explanation about how behavioral interviews work refer to [this video](https://drive.google.com/a/gitlab.com/file/d/0B0peM56mhGBEcXk2N2U3b05NNWc/view?usp=sharing) (GitLab's internal use only)

### Interview feedback

Always leave feedback, this will help everyone to understand what happened and how you came to your decision.

One way of writing the feedback is as follows:

> Vote: Inclined, Semi-Inclined or Not Inclined to Hire

> Requirements Score : Rate a candidate as 1-5 out of 5
> Values Alignment Score : Rate a candidate as a 1-5 out of 5

>  Summary: Include your general impressions, a brief description on what have experienced during interviews, where you stand and why you have this prospective. Bullet points are fine.
Pros: What is good and where did you hear it during the interview?
Cons: Where are the candidate's weaknesses specific to the requirements of the job? Identify how the candidate fell short of your expectations and why. Consider that some things can be taught or learned.
Interview notes: What questions were asked and what story did you got back? Ex.
>   Tell me about a time when you did X
>   The candidate told me a story when she was working in his current gig doing... They had to do... It went well because she took the leadership and .... In the end they increased their sales in a 30%, she measured it by doing ...

The vote and score are critical as they tell the rest of the hiring team your opinion on this hire.

Scoring can be defined as follows:

  - 5 - Certainly would hire (meets all requirements, aligns with values, no flags)
  - 4 - Likely to hire (meets most requirements, aligns with values)
  - 3 - Neutral (may meet some requirements, has some yellow flags)
  - 2 - Not likely to hire (meets few requirements, has many yellow flags, may not align with values well)
  - 1 - Would not hire (does not meet requirements, red flags, not aligned with values)

## Rejecting applicants

1. At any time during the hiring process the applicant can be rejected.
1. The applicant should always be notified of this. The employment team is primarily
responsible for declining candidates.
1. If the applicant asks for further feedback only offer frank feedback. This
is hard, but it is part of our [company values](/handbook/values).
    * All feedback should be constructive and said in a positive manner. Keep it short and sweet.
    * Feedback should always be applicable to the skill set of the position the candidate applied and interviewed for.
    * Feedback and rejection should always be based on the job requirements.
    * If you feel uncomfortable providing feedback for whatever reason, reach out to People Ops for assistance.
1. If people argue with the feedback that we provided:
    * Do not argue with or acknowledge the validity of the contents of the feedback.
    * Share their feedback with the people involved in the interviews and the decision.
    * Template text: "I've shared your feedback with the people involved in the interviews and the decision. We do not expect to revert the decision based on your feedback. In our hiring process we tend to error on being too cautious. We rather reject someone by mistake than hire someone by mistake, since a wrong hire is much more disruptive. Organizations can reject people with great potential http://thehustle.co/whatsapp-founder-got-rejected-by-both-twitter-and-facebook-before-19-billion-buyout so please don't be discouraged from seeking a great job."
1. The employment team will send out an inquiry to all candidates to gather feedback after they have exited the hiring process. The feedback survey should be sent out within 2 days after the applicant has been notified of the rejection or hire.
   * People Ops will review all feedback and use it to improve the hiring process.

## Hiring Package

The employment team will create an employee approval package for CEO for each candidate in Workable after their interview with an executive and their reference checks are completed. The CEO will review the approval package and tag the candidate as Approve, Question, Interview, Decline.

   * Approve = Make the offer
   * Question = Discuss offer with hiring manager
   * Interview = Proceed with final CEO interview
   * Decline = Decline candidate

Leave a comment in Workable with below hiring package template filled out and cc the hiring manager and CEO. Ping the CEO in chat with "Hiring approval needed for [Position]" with a link to the Workable comment.

   - Request approval for the following hire:
   - Interviewers and their score:
   - Flags raised:
   - Location:
   - Candidate's salary expectation beginning of process:
   - Candidate's salary expectation beginning of process:
   - Suggested Comp and stock options:
   - Comp Calculator url:
   - Hiring manager:
   - In case of interview is it OK to make the offer as CEO:

## Getting Offers and Contracts Ready, Reviewed, and Signed
{: #prep-contracts}

Offers made to new team members should be documented in Workable through the email thread between the person authorized to make the offer and the applicant.

1. Email example is in the "Offer letter" template in Workable. When using the template:
   1. make sure that you offer the correct [contract type and entity](/handbook/contracts/#how-to-use), ask People Ops if in doubt;
   1. include the People Ops alias in the cc (when you are ready for a written contract to be made), and
   1. change the subject line of the email. The default subject line in Workable is "{position name} - GitLab", but when making multiple hires for the same position, this can cause confusion in a Gmail inbox that collects conversation threads based on subject line. So for example make it "{first name of applicant} - offer for {position name} at GitLab" (this is something that we cannot do in the Workable template, unfortunately).
   1. Note: the number of proposed stock options must always be mentioned specifically, even when it is 0.
1. One person from People Operations will reply-to-all to everyone in the thread (including the applicant) to confirm that they will make the contract. Speed matters: if you are in People Operations and you can tackle this, then raise your hand and hit reply-all.
1. This person from People Operations
   1. checks all aspects of the offer:
      - was it approved by the CEO?
      - do the contract type and entity make sense?
      - is it clear how many (if any) stock options this person should receive?
      - is all necessary information (start date, salary, location, etc.) clearly available and agreed to?
   1. makes the contract based on the details found in the Workable platform, using reply-all to gather any missing pieces of information,
   1. has the contract reviewed and approved by another member of People Ops as a quality check. The People Ops team member who did not create the contract will conduct the quality check. Backups for approval are the People Ops Generalist, then the People Ops Specialist, and lastly the Sr. Director of People Operations.
   1. stages the contract in HelloSign and emails offer to the signing parties, cc People Ops
1. When the contract is signed, the People Ops team member should move the candidate in Workable to the "Hired" bucket of the "Hired Team Members (admin access only)" job. This accomplishes two goals. First, thanks to an integration between Workable and BambooHR, it will automatically add an entry for the new team member in BambooHR. However, in the automatic move, "self-service" is switched off in BambooHR by default, so this must be switched on explicitly within BambooHR. Second, by keeping the application and interview notes of successful applicants in a more restricted area of Workable we are able to maintain confidentiality if/when the applicant becomes involved in the hiring process for their peers.
1. This same person from People Operations files the signed contract in the appropriate place, and starts the [**onboarding issue**](/handbook/general-onboarding/).
1. Candidates will start the onboarding process no more than 30 days before her/his start date.

Note for People Operations:<br>
- the type of contract required (employee or contractor; BV or Inc) is clarified by the guideline on the
[Contracts page](/handbook/contracts).
- Onboarding info for the People Ops system, BambooHR, can be found on the [People Ops](/handbook/people-operations) page.

## CEO Interview Questions

The questions are available in a [Google form](https://docs.google.com/forms/d/1lBq_oXaqpQRs-SeEs3EvpxFGK55Enqn_nzkLq2l3Rwg/viewform) which can be used to save time during the actual interview.
All candidates are asked to fill out the form when they are scheduled for an interview with our CEO to discuss during their call with the CEO.

## Reference calls

As part of our hiring process we will ask applicants to provide us with two or more
references to contact. These reference calls are completed by our employment team following [these guidelines](http://www.bothsidesofthetable.com/2014/04/06/how-to-make-better-reference-calls/).

## Background Checks

Team members in the certain positions must partake in a [background check](/handbook/people-operations/#background-checks), which covers criminal and employment history.

Candidates who are in Support Engineering, Customer Success, People Ops, Finance, Sales (client-dependent), and the Executive team will be sent the link to the background check after the offer stage. The new team member may start with GitLab prior to the returned check, but their continued employment will be contingent on a clear returned check.

## Visas & Work Permits

GitLab does not offer full sponsorship for obtaining a H1B visa at this time.
If you already have an H1B visa and were hired, based on proven performance in the role, we would review and consider providing support for transferring your existing H1B. For questions on how to go about transferring an H1B Visa please see [People Operations](https://about.gitlab.com/handbook/people-operations/visas/#h1b-visa-processing).

For work permits and visas in The Netherlands please refer to [Dutch Work Permits](https://about.gitlab.com/handbook/people-operations/#dutch-work-permits) in our handbook.
