---
title: "Turn-Key GitLab Enterprise Kubernetes Clusters, Backup, Trusted Charts — All in Less than 10 Minutes"
author: Matt Baldwin 
author_twitter: baldwinmathew
categories: integration
image_title: '/images/blogimages/stackpoint-gitlab-integration.png'
description: "StackPointCloud partners with GitLab to create a simple, turn-key experience for developers who want to move faster into production with their apps."
twitter_image: '/images/blogpimages/stackpoint-gitlab-integration.png'
cta_button_text: 'Join the live demo' 
cta_button_link: 'http://get.gitlab.com/stack-point-gitlab/?&utm_source=blog&utm_campaign=stackpoint+announcement'
guest: true
---

[Stackpoint.io](https://stackpointcloud.com/) is excited to announce we’ve worked with GitLab to enable an end-to-end turn-key solution that will help developers move even faster from idea to production. 

<!-- more -->

Stackpoint.io advances the mandate of allowing developers to continue to focus on building product, leaving configuring the tooling to GitLab and Stackpoint.io. With this release, together, users can manage and collaborate on their clusters and ensure Gitlab EE is operating correctly — all in a turn-key, developer-friendly way.

Our Kubernetes cloud management platform now allows you to: 

* Build a GitLab EE Kubernetes cluster on the cloud of your choice - Google Compute, AWS, or Azure-in three easy steps. 
* Deploy GitLab EE to an existing Kubernetes cluster.
* Upgrade your GitLab EE Kubernetes cluster in one click.
* Set up a Kubernetes backup schedule-store in Google or Amazon, recover anywhere.
* Get all your operational components, pre-configured, at build or run time-Sysdig for monitoring, Twistlock for security, Elasticsearch with Fluentd and Kibana for logging, and more.
* Allow your developers quick and easy access to operational tools, trimmed down. For example, they can dive into their cluster’s Prometheus metrics – one click.

![StackPoint integration with GitLab](/images/blogimages/stackpoint-integration.png)

Our GitLab integration not only allows you to run a self-healing deployment of GitLab EE on Kubernetes, but we’ve also integrated Docker Registry automatically, if you’re running on AWS we set up ELB for you and secure it all with Let’s Encrypt. 

## Get Started 

1. Get a new GitLab EE Kubernetes cluster up, running, and configured for production within 10 minutes. 

2. Deploy your first app to K8S using GitLab. 

3. Schedule your protection of your cluster.

Give it a shot [now](https://stackpoint.io/#/clusters/new?provider=aws&solution=gitlab_ee).